import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DatastoragePage } from './datastorage';

@NgModule({
  declarations: [
    DatastoragePage,
  ],
  imports: [
    IonicPageModule.forChild(DatastoragePage),
  ],
})
export class DatastoragePageModule {}
