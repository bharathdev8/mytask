import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from "angularfire2/database-deprecated";
import {EditPage} from "../edit/edit";
import { EventDetails } from '../../models/event-details/event-details.interface';
import {GeolocationPage} from "../geolocation/geolocation";

/**
 * Generated class for the DatastoragePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-datastorage',
  templateUrl: 'datastorage.html',
})
export class DatastoragePage {

    eventDetailsRef$: FirebaseListObservable<EventDetails[]>

  constructor(public navCtrl: NavController, public navParams: NavParams,  private database: AngularFireDatabase, private actionSheetCtrl: ActionSheetController) {
      this.eventDetailsRef$ = this.database.list('datastorage');
    }

    selectEventDetails(eventDetails: EventDetails) {
        this.actionSheetCtrl.create({
            title: `${eventDetails.eventTitle}`,
            buttons: [
                {
                    text: 'Edit',
                    handler: () => {
                        this.navCtrl.push(EditPage,
                            { eventDetailsId: eventDetails.$key });

                    }
                },
                {
                    text: 'Delete',
                    role: 'destructive',
                    handler: () => {
                        this.eventDetailsRef$.remove(eventDetails.$key);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log("Cancelled");
                    }
                }
            ]
        }).present();
    }

    navigateToAddAddeventPage() {
        this.navCtrl.push(GeolocationPage);
    }
}
