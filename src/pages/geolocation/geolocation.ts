import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from "angularfire2/database-deprecated";
import { EventDetails } from '../../models/event-details/event-details.interface';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
    selector: 'page-geolocation',
    templateUrl: 'geolocation.html',
})
export class GeolocationPage {

    lat: any;
    lng: any;

    eventDetails = {} as EventDetails;

    eventDetailsRef$: FirebaseListObservable<EventDetails[]>

    constructor(public navCtrl: NavController, public navParams: NavParams, private database: AngularFireDatabase, public geolocation: Geolocation) {
        this.eventDetailsRef$ = this.database.list('datastorage');
    }

    public ngOnInit() {

    }

    ionViewDidLoad()
    {
        this.geolocation.getCurrentPosition().then( pos => {
            this.lat=pos.coords.latitude;
            this.lng=pos.coords.longitude;
        }).catch( err=> console.log(err));
    }

    addEventDetails(eventDetails: EventDetails) {
        this.eventDetailsRef$.push({
            eventTitle: this.eventDetails.eventTitle,
            eventContent: this.eventDetails.eventContent,
        });

        this.eventDetails = {} as EventDetails;
    }
}
