import { Component } from '@angular/core';
import {CameraPage} from "../camera/camera";
import {GeolocationPage} from "../geolocation/geolocation";
import {DatastoragePage} from "../datastorage/datastorage";
import {EditPage} from "../edit/edit";

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  templateUrl: 'tabs.html',
})
export class TabsPage {

    tab1Root = CameraPage;
    tab2Root = GeolocationPage;
    tab3Root = DatastoragePage;
    tab4Root = EditPage;

  constructor() {
  }
}
