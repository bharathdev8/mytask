import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from "angularfire2/database-deprecated";
import { Subscription } from 'rxjs/Subscription';
import { EventDetails } from '../../models/event-details/event-details.interface';


/**
 * Generated class for the EditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {

    eventDetailsSubscription: Subscription;
    eventDetailsRef$: FirebaseObjectObservable<EventDetails>;
    eventDetails = {} as EventDetails;

  constructor(public navCtrl: NavController, public navParams: NavParams, private database: AngularFireDatabase, private plt: Platform) {

      const eventDetailsId = this.navParams.get('eventDetailsId');

      console.log(eventDetailsId);

      this.eventDetailsRef$ = this.database.object(`datastorage/${eventDetailsId}`);

      this.eventDetailsSubscription =
          this.eventDetailsRef$.subscribe(
              eventDetails => this.eventDetails = eventDetails);
  }

    editEventDetails(eventDetails: EventDetails) {
        this.eventDetailsRef$.update(eventDetails);
        this.navCtrl.pop();
    }

    ionViewWillLeave() {
        this.eventDetailsSubscription.unsubscribe();
    }
}
