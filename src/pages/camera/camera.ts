import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Camera, CameraOptions} from '@ionic-native/camera';
import {PhotoViewer} from "ionic-native";


@IonicPage()
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {

    images = ['Avengers.jpg', 'Wonder Woman.jpg', 'King Arthur.jpg', 'Big Hero.jpg'];

    public photos: any;

    public base64Image: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private photoViewer: PhotoViewer, private alertCtrl: AlertController) {
  }

    ngOnInit() {
        this.photos = [];
    }

    cameraImage() {
        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }

        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = "data:image/jpeg;base64," + imageData;
            this.photos.push(this.base64Image);
            this.photos.reverse()
        }, (err) => {
        });
    }

    openGallery() {
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum:false
        }).then((imageData) => {
            this.base64Image = "data:image/jpeg;base64," + imageData;
        }, (err) => {
            console.log(err);
        });
    }

    deletePhoto(index) {
        let confirm = this.alertCtrl.create({
            title: 'Confirm to delete',
            message: '',
            buttons: [
                {
                    text: 'NO',
                    handler: () => {

                    }
                },
                {
                    text: 'YES',
                    handler: () => {
                        this.photos.splice(index, 1);
                    }
                }]
        });
        confirm.present();
    }
}
