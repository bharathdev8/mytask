export interface EventDetails {
    $key?: string,
    eventTitle: string;
    eventContent: string;
}