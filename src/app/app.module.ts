import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import {PhotoViewer} from "ionic-native";
import { IonicImageViewerModule } from 'ionic-img-viewer';
import {AngularFireModule} from "angularfire2";
import {firebaseConfig} from "./credentials.backup"
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {CameraPage} from "../pages/camera/camera";
import {GeolocationPage} from "../pages/geolocation/geolocation";
import {DatastoragePage} from "../pages/datastorage/datastorage";
import {EditPage} from "../pages/edit/edit";
import {TabsPage} from "../pages/tabs/tabs";
import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
    declarations: [
        MyApp,
        CameraPage,
        GeolocationPage,
        DatastoragePage,
        EditPage,
        TabsPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicImageViewerModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        HttpClientModule,
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        CameraPage,
        GeolocationPage,
        DatastoragePage,
        EditPage,
        TabsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Camera,
        PhotoViewer,
        AngularFireDatabase,
        Geolocation
    ]
})
export class AppModule {
}
